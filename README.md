# barebone

barebone is a veryyyyyyy simple rust proc macro for freestanding.

## Features

1. Veryyyyyy simple but verrrrry usefulllll.

```rust

#![no_std]
#![no_main]

use dos::Interupt;

fn main() -> i32 {
    libc::puts(b"Hello, World!");
    return 0;
}

#[barebone::export_cffi]
fn _start() {
    let mut reg = dos::REG::new();
    reg.eax.value = main() as u32;
    reg.eax.reg16.reg8.h = 0x4c;
    reg.int0x21();
}

barebone::panic_handler!();

```

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <http://www.facebook.com/dead-root>
