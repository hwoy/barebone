use proc_macro::TokenStream;
use std::str::FromStr;

#[proc_macro_attribute]
pub fn export_cffi(_: TokenStream, item: TokenStream) -> TokenStream {
    let token = "#[no_mangle] pub extern \"C\" ";

    let mut token = TokenStream::from_str(token).unwrap();
    token.extend(item);

    token
}

#[proc_macro]
pub fn panic_handler(_: TokenStream) -> TokenStream {
    let token =
        "use core::panic::PanicInfo; #[panic_handler] fn panic(_: &PanicInfo) -> ! {loop {}}";
    TokenStream::from_str(token).unwrap()
}
